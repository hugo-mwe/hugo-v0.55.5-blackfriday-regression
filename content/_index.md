+++
title = "Source block without list syntax in a list"
date = 2017-08-01
tags = ["src-block", "lists", "hyphen"]
draft = false
+++

Ref: https://discourse.gohugo.io/t/rendering-code-blocks-properly-from-md-files/19126/22?u=kaushalmodi

-   List item 1

    ```md
    *abc*
    /def/
    =def=
    ```
-   List item 2

## Rendering of above in Hugo v0.55.4

```html
<ul>
<li><p>List item 1</p>

<pre><code class="language-md">*abc*
/def/
=def=
</code></pre></li>

<li><p>List item 2</p></li>
</ul>
```

<ul>
<li><p>List item 1</p>

<pre><code class="language-md">*abc*
/def/
=def=
</code></pre></li>

<li><p>List item 2</p></li>
</ul>

## Rendering of above in Hugo v0.55.5 (regression)

```html

<ul>
<li><p>List item 1</p>

<pre><code class="language-md">*abc*
/def/
=def=
</code></pre>

<ul>
<li>List item 2</li>
</ul></li>
</ul>
```

<ul>
<li><p>List item 1</p>

<pre><code class="language-md">*abc*
/def/
=def=
</code></pre>

<ul>
<li>List item 2</li>
</ul></li>
</ul>
